PyrootCK.patchers package
=========================

Submodules
----------

PyrootCK.patchers.RooAbsArg module
----------------------------------

.. automodule:: PyrootCK.patchers.RooAbsArg
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.RooDataHist module
------------------------------------

.. automodule:: PyrootCK.patchers.RooDataHist
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.RooFitResult module
-------------------------------------

.. automodule:: PyrootCK.patchers.RooFitResult
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.RooRealVar module
-----------------------------------

.. automodule:: PyrootCK.patchers.RooRealVar
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.RooWorkspace module
-------------------------------------

.. automodule:: PyrootCK.patchers.RooWorkspace
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TAxis module
------------------------------

.. automodule:: PyrootCK.patchers.TAxis
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TFile module
------------------------------

.. automodule:: PyrootCK.patchers.TFile
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TGraphAsymmErrors module
------------------------------------------

.. automodule:: PyrootCK.patchers.TGraphAsymmErrors
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TGraphErrors module
-------------------------------------

.. automodule:: PyrootCK.patchers.TGraphErrors
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TH1 module
----------------------------

.. automodule:: PyrootCK.patchers.TH1
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TH2 module
----------------------------

.. automodule:: PyrootCK.patchers.TH2
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TList module
------------------------------

.. automodule:: PyrootCK.patchers.TList
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TMatrix module
--------------------------------

.. automodule:: PyrootCK.patchers.TMatrix
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TMinuit module
--------------------------------

.. automodule:: PyrootCK.patchers.TMinuit
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TMultiGraph module
------------------------------------

.. automodule:: PyrootCK.patchers.TMultiGraph
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TNamed module
-------------------------------

.. automodule:: PyrootCK.patchers.TNamed
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.patchers.TTree module
------------------------------

.. automodule:: PyrootCK.patchers.TTree
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: PyrootCK.patchers
    :members:
    :undoc-members:
    :show-inheritance:
