PyrootCK package
================

Subpackages
-----------

.. toctree::

    PyrootCK.mathutils
    PyrootCK.patch_uncertainties
    PyrootCK.patchers

Submodules
----------

PyrootCK.ioutils module
-----------------------

.. automodule:: PyrootCK.ioutils
    :members:
    :undoc-members:
    :show-inheritance:

PyrootCK.tmvautils module
-------------------------

.. automodule:: PyrootCK.tmvautils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: PyrootCK
    :members:
    :undoc-members:
    :show-inheritance:
